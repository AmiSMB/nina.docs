N.I.N.A. organizes different functional areas in its user interface into tabs. In some instances, a tab may be broken up into sub-tabs or sub-windows.  An example of this is the Equipment tab, or in the Imaging tab.

The goal with tabs is to visually and functionally separate distinct areas of the application to avoid clutter. Descriptions of each tab and their constituent functions are in the following sections.

![Tabs](../images/tabs/overview1.png)
