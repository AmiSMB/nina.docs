The Filterwheel  Tab lets you connect an ASCOM-compatible filterwheel

![Filterwheel](../../images/tabs/equipments_fw.png)

1. Filter wheel information
2. Select and change filter
3. List of current filters as imported from ASCOM driver

