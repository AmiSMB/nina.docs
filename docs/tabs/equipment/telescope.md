The Rotator Tab lets you connect an ASCOM-compatible tlescope mount. 

![Telescope](../../images/tabs/equipment_scope.png)

1. Telescope information 
2. Define manual target coordinates
3. Slew to manual target coordinates
4. Define movement rate
5. Manual movement commands
6. Park mount
7. Set current position as parking position

