The N.I.N.A. source code itself is licensed under the Mozilla Public License Version 2.0. 
However it depends on various third party libraries which carry their own copyright notices and license terms which are explained below.

| Library              | Project Site       | License    |
| -------------------- |:-----------|:----------|
| AsyncEnumerator          | [https://github.com/Dasync/AsyncEnumerable]() | [The MIT License](https://opensource.org/licenses/MIT)                   |
| Newtonsoft.Json          | [https://www.newtonsoft.com/json](https://www.newtonsoft.com/json) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| Nito.AsyncEx             | [https://github.com/StephenCleary/AsyncEx](https://github.com/StephenCleary/AsyncEx) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| NJsonSchema              | [https://github.com/RicoSuter/NJsonSchema](https://github.com/RicoSuter/NJsonSchema) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| OxyPlot                  | [https://oxyplot.github.io/](https://oxyplot.github.io/) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| SHA3                     | [https://bitbucket.org/jdluzen/sha3](https://bitbucket.org/jdluzen/sha3) | [The MIT License](https://opensource.org/licenses/MIT)                   |
| lz4net                   | [https://github.com/MiloszKrajewski/lz4net](https://github.com/MiloszKrajewski/lz4net) | [The 2-Clause BSD License](https://opensource.org/licenses/BSD-2-Clause) |
| CSharpFITS               | [http://vo.iucaa.ernet.in/~voi/CSharpFITS.html](http://vo.iucaa.ernet.in/~voi/CSharpFITS.html) | [The 3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause) |
| WPFToolkit (v3.5.0)      | [https://github.com/xceedsoftware/wpftoolkit](https://github.com/xceedsoftware/wpftoolkit) | [Microsoft Public License (MS-PL)](https://opensource.org/licenses/MS-PL) |
| EntityFramework          | [https://go.microsoft.com/fwlink/?LinkID=263480](https://go.microsoft.com/fwlink/?LinkID=263480) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| SharpGIS.NmeaParser      | [https://dotmorten.github.io/NmeaParser/](https://dotmorten.github.io/NmeaParser/) | [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| Accord                   | [http://accord-framework.net/](http://accord-framework.net/) | [GNU Lesser General Public License version 2.1](https://www.gnu.de/documents/lgpl-2.1.de.html) |
| ToastNotifications       | [https://github.com/raflop/ToastNotifications](https://github.com/raflop/ToastNotifications) | [GNU Lesser General Public License version 3](https://www.gnu.de/documents/lgpl-3.0.de.html) |
| NikonCSWrapper           | [https://sourceforge.net/projects/nikoncswrapper/](https://sourceforge.net/projects/nikoncswrapper/) | [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/de/legalcode) |
| System.Data.SQLite       | [https://system.data.sqlite.org/](https://system.data.sqlite.org/) | [https://www.sqlite.org/copyright.html](https://www.sqlite.org/copyright.html)
| zlib                     | [https://dotnetzip.codeplex.com/](https://dotnetzip.codeplex.com/) | [zlib License](https://opensource.org/licenses/Zlib) |
| VVVV.FreeImage           | [http://freeimage.sourceforge.net/](http://freeimage.sourceforge.net/) | [FreeImage Public License - Version 1.0](http://freeimage.sourceforge.net/freeimage-license.txt) |



